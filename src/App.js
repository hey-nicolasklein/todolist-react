import React from 'react';
import {useState, useReducer, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';

const initalTodos = [
  {
    id: '1',
    task: 'Understand useReducer',
    completed: false
  },
  {
    id: '2',
    task: 'Listen to 808Mafia',
    completed: true
  }
];


const todoReducer = (state, action) => {
    switch(action.type){
      case 'DO_TODO':
        return state.map(todo =>{
          if(todo.id === action.id){
            return {...todo, completed: true}
          }
          else return todo;
        })
      case 'UNDO_TODO':
        return state.map(todo =>{
          if(todo.id === action.id){
            return {...todo, completed: false}
          }
          else return todo
        })

      case 'ADD_TODO':
        return [...state, action.item];
      default:
        return state;
    }
}

const App = () => {

  const [todos, dispatch] = useReducer(todoReducer, initalTodos);
  const [newTaskName, setNewTaskName] = useState("");
  const [idCount, setIDCount] = useState(3);


  const handleChange = (todo) => {
    dispatch({type: todo.completed ? 'UNDO_TODO' : 'DO_TODO', id: todo.id})
  };


  const handleAdd = e => {

    e.preventDefault();

    const todo = {
      id: idCount.toString(),
      task: newTaskName,
      completed: false
    }

    setIDCount(count => count + 1);
    setNewTaskName("");

    dispatch({type: 'ADD_TODO', id: todo.id, item: todo})
  }

  const updateNewTaskName = e => {
    setNewTaskName(e.target.value);
  }

  return (
    <div class="frame">
    <div class="container">
      <div class="todo-header">
        <h1 style={{display: 'inline'}}>My todos</h1>
        <h4 id="currentTodos">Current todos: {
          todos.filter(e => e.completed === false).reduce((prev, current) => prev+1,0)
          }</h4>
      </div>
      
      <div class="list-todo">
      {
        todos.filter(e => e.completed === false).map(todo => (
          <div class="item">
            <label>
              <input 
              type="checkbox"
              checked={todo.completed}
              onChange={() =>handleChange(todo)}
              />
              {todo.task}
            </label>
          </div>
        ))
      }
      </div>
      <h2>Done</h2>
      <div class="list-done">
      {
        todos.filter(e => e.completed === true).map(todo => (
          <div class="item lineThrough">
            <label>
              <input 
              type="checkbox"
              checked={todo.completed}
              onChange={() =>handleChange(todo)}
              />
              {todo.task}
            </label>
          </div>
        ))
      }
      </div>
    
    <form class="input-container" onSubmit={handleAdd}>
      <input type="text" value={newTaskName} onChange={updateNewTaskName}/>
      <button type="submit">
        AddTask
      </button>
    </form>
    </div>
    </div>
  )
}

export default App;
